package com.emusicshop.controller;

import com.emusicshop.dao.ProductDAO;
import com.emusicshop.model.Product;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;

@Controller
public class HomeController {

    private final ProductDAO productDAO;

    @Autowired
    public HomeController(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @RequestMapping("/")
    public String home() {

        return "home";
    }

    @RequestMapping("/productList")
    public String getProducts(Model model) {

        List<Product> products = productDAO.getAllProducts();
        Collections.reverse(products);
        encodeListToBase64(products);
        model.addAttribute("products", products);

        return "productList";
    }

    @RequestMapping("/productList/viewProduct/{productId}")
    public String viewProduct(@PathVariable String productId, Model model) {

        Product product = productDAO.getProductById(Integer.parseInt(productId));

        product.setBase64EncodedImage(encodeToBase64(product.getImageFile()));
        model.addAttribute(product);

        return "viewProduct";
    }

    @RequestMapping("/admin")
    public String adminPage() {
        return "admin";
    }

    @RequestMapping("/admin/productInventory")
    public String productInventory(Model model) {

        List<Product> products = productDAO.getAllProducts();
        Collections.reverse(products);
        encodeListToBase64(products);
        model.addAttribute("products", products);

        return "productInventory";
    }

    @RequestMapping("/admin/productInventory/addProduct")
    public String addProduct(Model model) {

        Product product = new Product();
        model.addAttribute(product);

        return "addProduct";
    }

    //  @RequestMapping(value = "/admin/productInventory/addProduct", method = RequestMethod.POST)
    @PostMapping("/admin/productInventory/addProduct")
    public String addProductPost(@Valid @ModelAttribute("product") Product product, BindingResult bindingResult,
                                 @RequestParam("multipartImageFile") MultipartFile multipartImageFile) {
        if (bindingResult.hasErrors()) {
            return "addProduct";
        }

        try {
            if (!multipartImageFile.isEmpty())
                product.setImageFile(multipartImageFile.getBytes());
            productDAO.addOrEditProduct(product);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "redirect:/admin/productInventory";
    }

    @GetMapping("/admin/productInventory/editProduct/{productId}")
    public String editProduct(@PathVariable String productId, Model model) {
        Product product = productDAO.getProductById(Integer.parseInt(productId));
        product.setBase64EncodedImage(encodeToBase64(product.getImageFile()));
        model.addAttribute("product", product);

        return "editProduct";
    }

    @PostMapping("/admin/productInventory/editProduct")
    public String editProductPost(@ModelAttribute("product") Product product, BindingResult bindingResult,
                                  @RequestParam("multipartImageFile") MultipartFile multipartfile) {
        if (bindingResult.hasErrors()) {
            return "editProduct";
        }

        if (!multipartfile.isEmpty()) {
            try {
                product.setImageFile(multipartfile.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (!product.getBase64EncodedImage().isEmpty()) {
            product.setImageFile(Base64.decodeBase64(product.getBase64EncodedImage()));
        }

        productDAO.addOrEditProduct(product);

        return "redirect: /admin/productInventory";
    }

    @GetMapping("/admin/productInventory/deleteProduct/{productId}")
    public String deleteProduct(@PathVariable String productId) {

        productDAO.deleteProduct(Integer.parseInt(productId));

        return "redirect:/admin/productInventory";
    }

    private void encodeListToBase64(List<Product> products) {
        products.forEach
                (prod -> {
                            byte[] imageFile = prod.getImageFile();
                            String encodedImageFile = encodeToBase64(imageFile);
                            prod.setBase64EncodedImage(encodedImageFile);
                        }
                );
    }

    private String encodeToBase64(byte[] imageFileByte) {
        if (imageFileByte != null && imageFileByte.length > 0) {
            byte[] encodedBase64ImageByte = Base64.encodeBase64(imageFileByte);
            try {
                return new String(encodedBase64ImageByte, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
