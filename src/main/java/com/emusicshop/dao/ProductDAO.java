package com.emusicshop.dao;

import com.emusicshop.model.Product;

import java.util.List;

public interface ProductDAO {

    void addOrEditProduct(Product product);

    Product getProductById(int productId);

    List<Product> getAllProducts();

    void deleteProduct(int productId);
}
