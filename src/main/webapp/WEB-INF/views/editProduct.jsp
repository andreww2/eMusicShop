<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="template/header.jsp" %>


<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Edit product</h1>
            <p class="lead">On this page you can edit products in your offer.</p>
        </div>

        <form:form id="imageForm" action="${pageContext.request.contextPath}/admin/productInventory/editProduct"
                   method="post" commandName="product" enctype="multipart/form-data">

        <form:hidden path="productId" value="${product.productId}"/>
        <form:hidden path="base64EncodedImage" value="${product.base64EncodedImage}"/>

        <div class="form-group">
            <label for="name">Name</label>
            <form:input path="productName" id="name" class="form-Control" value="${product.productName}"/>
        </div>

        <div class="form-group">
            <label for="manufacturer">Manufacturer</label>
            <form:input path="productManufacturer" id="manufacturer" class="form-Control"
                        value="${product.productManufacturer}"/>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <form:textarea path="productDescription" id="description" class="form-Control"
                           value="product.productDescription"/>
        </div>

        <div class="form-group">
            <label for="category">Category</label>
            <label class="checkbox-inline"><form:radiobutton path="productCategory" id="category"
                                                             value="Instrument"/>Instrument</label>
            <label class="checkbox-inline"><form:radiobutton path="productCategory" id="category"
                                                             value="Record"/>Record</label>
            <label class="checkbox-inline"><form:radiobutton path="productCategory" id="category"
                                                             value="Accessory"/>Accessory</label>
        </div>
        <div class="form-group">
            <label for="status">Status</label>
            <label class="checkbox-inline"><form:radiobutton path="productStatus" id="status"
                                                             value="active"/>Active</label>
            <label class="checkbox-inline"><form:radiobutton path="productStatus" id="status"
                                                             value="inactive"/>Inactive</label>
        </div>
        <div class="form-group">
            <label for="condition">Condition</label>
            <label class="checkbox-inline"><form:radiobutton path="productCondition" id="condition"
                                                             value="New"/>New</label>
            <label class="checkbox-inline"><form:radiobutton path="productCondition" id="condition"
                                                             value="Used"/>Used</label>
        </div>
        <div class="form-group">
            <label for="price">Price</label>
            <form:input path="productPrice" id="price" value="${product.productPrice} $" class="form-Control"/>
        </div>
        <div class="form-group">
            <label for="units">Units in stock</label>
            <form:errors path="productUnits" cssStyle="color: red"/>
            <form:input path="productUnits" id="units" class="form-Control"/>
        </div>
        <div class="form-group">
            <label for="productMultipartImage" class="control-label">Upload Image</label>
            <input name="multipartImageFile" id="productMultipartImage" type="file" class="form:input-large"/>
        </div>

        <br><br>
        <input type="submit" value="Submit" class="btn btn-success"/>
        <a href="<c:url value="/admin/productInventory"/>" class="btn btn-warning">Cancel</a>
        </form:form>

<%@include file="template/footer.jsp" %>