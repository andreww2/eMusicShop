<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@include file="template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Product</h1>
            <p class="lead">Product Details</p>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="data:image/jpg; image/png; image/jpeg; base64,${product.base64EncodedImage}" alt="image"
                             style="width: 100%;"/>
                </div>
                <div class="col-md-5">
                    <h3>${product.productName}</h3>
                    <p>${product.productDescription}</p>
                    <p><strong>Manufacturer</strong> : ${product.productManufacturer}</p>
                    <p><strong>Category</strong> : ${product.productManufacturer}</p>
                    <p><strong>Condition</strong> : ${product.productCondition}</p>
                    <h4>${product.productPrice} $</h4>
                </div>
            </div>
        </div>

<%@include file="template/footer.jsp" %>