<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@include file="template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Product inventory</h1>
            <p class="lead">All the available products just for you to maintain!</p>
        </div>

        <table class="table table-striped table-hover">
            <thead>
            <tr class="bg-success">
                <th>Photo Thumb</th>
                <th>Product Name</th>
                <th>Category</th>
                <th>Condition</th>
                <th>Price</th>
                <th style="width: 75px;"></th>
            </tr>
            </thead>
            <c:forEach items="${products}" var="product">
                <tr>
                    <td><img src="data:image/jpg; image/png; image/jpeg; base64,${product.base64EncodedImage}" alt="image"
                             style="width: 100%;" /></td>
                    <td>${product.productName}</td>
                    <td>${product.productCategory}</td>
                    <td>${product.productCondition}</td>
                    <td>${product.productPrice} $</td>
                    <td><a href="<spring:url value="/productList/viewProduct/${product.productId}"/>">
                        <span class="glyphicon glyphicon-link"></span>
                    </a>
                    <a href="<spring:url value="/admin/productInventory/editProduct/${product.productId}"/>">
                        <span class="glyphicon glyphicon-pencil"></span>
                    </a>
                        <a href="<spring:url value="/admin/productInventory/deleteProduct/${product.productId}"/>">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </td>
                </tr>
            </c:forEach>
        </table>

        <div>
            <a href="<c:url value="/admin/productInventory/addProduct"/>" class="btn btn-success">Add Product</a>
        </div>

        <br/>
<%@include file="template/footer.jsp" %>