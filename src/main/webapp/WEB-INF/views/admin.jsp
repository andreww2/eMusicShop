<%@include file="template/header.jsp" %>

<div class="container-wrapper">
    <div class="container">
        <div class="page-header">
            <h1>Administration page</h1>
            <p class="lead">Welcome to administration page!</p>
            <h3>
                <a href="<c:url value="/admin/productInventory"/>" class="btn btn-primary">Product Inventory</a>
            </h3>
            <p>Here you can view, check and modify the product list.</p>
        </div>

<%@include file="template/footer.jsp" %>